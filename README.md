# Cucumber mobile test framework

This is UI test framework for Android, iOS platforms with using Cucumber

# How to run (bash script example, please see as raw)

#! /bin/bash
cd ~/ANDROID_PORTALS_APP
git pull
./gradlew clean assembleNurDebug
export BROWSERSTACK_USER=" ... "
export BROWSERSTACK_ACCESS_KEY=" ... "
export ANDROID_APP_URL=$(curl -u "$BROWSERSTACK_USER:$BROWSERSTACK_ACCESS_KEY" -X POST -F "file=@/Users/solovianov/ANDROID_PORTALS_APP/app/build/outputs/apk/nur/debug/app-nur-debug.apk" https://api-cloud.browserstack.com/app-automate/upload | grep -o 'bs://[^"]*')
cd ~/nur-mobile-cucumber
mvn clean install -q \
-Dspring.profiles.active="Android" \
-Dplatform.name="Android" \
-Dapp.file=$ANDROID_APP_URL \
-Ddevice.location="Cloud" \
-Ddevice.name="Samsung Galaxy Note 8" \
-D"cucumber.options= --tags @feed --tags ~@ignore"