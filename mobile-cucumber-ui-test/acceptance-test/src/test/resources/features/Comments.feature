@t
@comments
Feature: Comments

  Background:
    Given application is installed successfully

  @nur @android
  Scenario: 'Неподтвержденный пользователь видит сообщение об ошибке'
    Given interface language is 'RU'
    Then unverified user 'Developer2' cannot log in

  @nur @android
  Scenario Outline: 'Администратор успешно оставляет корневой комментарий'
    Given received a list of Posts
    And define news post for testing = <index>
    And get asset id for current news article
    And there are no test comments for news
    And interface language is 'RU'
    And user successfully logs in as confirmed user 'services'
    And user goes to feed Latest
    And the user finds and opens the news article
    When user sent the 'Test comment 1' comment under the current news
    And the 0 comment in the date field has the text 'На модерации'
    And pull to refresh
    Then comment 'Test comment 1' is present in the list of comments
    And the 0 comment in the date field has not the text 'На модерации'
    Examples:
      | index |
      | 20    |

  @nur @android @bugfix
  Scenario Outline: 'Администратор успешно оставляет корневой комментарий содержащий 2 символа'
    Given received a list of Posts
    And define news post for testing = <index>
    And get asset id for current news article
    And there are no test comments for news
    And interface language is 'RU'
    And user successfully logs in as confirmed user 'services'
    And user goes to feed Latest
    And the user finds and opens the news article
    When user sent the 'a' comment under the current news
    And the 0 comment in the date field has the text 'На модерации'
    And pull to refresh
    Then comment 'aa' is present in the list of comments
    And the 0 comment in the date field has not the text 'На модерации'
    Examples:
      | index |
      | 20    |

  @nur @android
  Scenario Outline: 'Подтвержденный пользователь оставляет корневой комментарий, который отправляется на модерацию'
    Given received a list of Posts
    And define news post for testing = <index>
    And get asset id for current news article
    And there are no test comments for news
    And interface language is 'RU'
    And user successfully logs in as confirmed user 'Developer1'
    And user goes to feed Latest
    And the user finds and opens the news article
    When user sent the 'Test comment 1' comment under the current news
    Then comment 'Test comment 1' is present in the list of comments
    And pull to refresh
    Then no comments are present on screen
    Examples:
      | index |
      | 20    |

  @nur @android
  Scenario Outline: 'Отображение корневых комментариев'
    Given received a list of Posts
    And define news post for testing = <index>
    And get asset id for current news article
    And there are no test comments for news
    And created 4 root comment as user 'services'
    And interface language is 'RU'
    And user successfully logs in as confirmed user 'services'
    And user goes to feed Latest
    And the user finds and opens the news article
    When user goes to comments screen
    And user refreshes the screen waiting for comments to appear if there are none
    Then the order and content of the root comments is as expected
    And the offset of the list of comments corresponds to the pattern
      | 0 |
      | 0 |
      | 0 |
      | 0 |
    Examples:
      | index |
      | 20    |

  @nur @android
  Scenario Outline: 'Подкомментарий к рутовому комментарию добавляется на 1 уровне вложения'
    Given received a list of Posts
    And define news post for testing = <index>
    And get asset id for current news article
    And there are no test comments for news
    And created 1 root comment as user 'services'
    And interface language is 'RU'
    And user successfully logs in as confirmed user 'services'
    And user goes to feed Latest
    And the user finds and opens the news article
    When user goes to comments screen
    And user refreshes the screen waiting for comments to appear if there are none
    Then the order and content of the root and sub comments from index 1 is as expected
    When user setting up the comment 'Test comment 1' to comment with index 0
    And pull to refresh
    Then comment 'Test comment 1' is present in the list of comments
    And the offset of the list of comments corresponds to the pattern
      | 0 |
      | 1 |
    Examples:
      | index |
      | 20    |

  @nur @android
  Scenario Outline: 'Отображение 13 корневых комментариев (скролл и проверка пагинации)'
    Given received a list of Posts
    And define news post for testing = <index>
    And get asset id for current news article
    And there are no test comments for news
    And created 13 root comment as user 'services'
    And interface language is 'RU'
    And user successfully logs in as confirmed user 'services'
    And user goes to feed Latest
    And the user finds and opens the news article
    When user goes to comments screen
    And user refreshes the screen waiting for comments to appear if there are none
    And scroll down the screen to activate pagination
    Then the user scrolls and sees all comments in the correct order
    Examples:
      | index |
      | 20    |

  @nur @android
  Scenario Outline: 'Отображение 3 подкомментариев 1-го уровня к корневому'
    Given received a list of Posts
    And define news post for testing = <index>
    And get asset id for current news article
    And there are no test comments for news
    And created 3 sub comments to 0 root on one level as user 'services'
    And interface language is 'RU'
    And user successfully logs in as confirmed user 'services'
    And user goes to feed Latest
    And the user finds and opens the news article
    When user goes to comments screen
    And user refreshes the screen waiting for comments to appear if there are none
    Then the order and content of the root and sub comments from index 1 is as expected
    And the offset of the list of comments corresponds to the pattern
      | 0 |
      | 1 |
      | 1 |
      | 1 |
    Examples:
      | index |
      | 20    |

  @nur @android
  Scenario Outline: 'Отображение 1-2-3-го уровня вложенных комментариев к корневому'
    Given received a list of Posts
    And define news post for testing = <index>
    And get asset id for current news article
    And there are no test comments for news
    And created 3 sub comments to 0 root as user 'services'
    And interface language is 'RU'
    And user successfully logs in as confirmed user 'services'
    And user goes to feed Latest
    And the user finds and opens the news article
    When user goes to comments screen
    And user refreshes the screen waiting for comments to appear if there are none
#    Then the order and content of the root and sub comments from index 1 is as expected
    Then the offset of the list of comments corresponds to the pattern
      | 0 |
      | 1 |
      | 2 |
      | 3 |
    Examples:
      | index |
      | 20    |

  @nur @android
  Scenario Outline: 'Подкомментарий к 3 уровню вложения добавляется на том же уровне'
    Given received a list of Posts
    And define news post for testing = <index>
    And get asset id for current news article
    And there are no test comments for news
    And created 3 sub comments to 0 root as user 'services'
    And interface language is 'RU'
    And user successfully logs in as confirmed user 'services'
    And user goes to feed Latest
    And the user finds and opens the news article
    And user goes to comments screen
    And user refreshes the screen waiting for comments to appear if there are none
#    Then the order and content of the root and sub comments from index 1 is as expected
    When user setting up the comment 'Test comment 1' to comment with index 3
    And pull to refresh
    Then the offset of the list of comments corresponds to the pattern
      | 0 |
      | 1 |
      | 2 |
      | 3 |
      | 3 |
    Examples:
      | index |
      | 20    |

  @nur @android
  Scenario Outline: 'Пользователь видит 3 из 4 подкомментариев к 1 корневому и текст Загрузить еще'
    Given received a list of Posts
    And define news post for testing = <index>
    And get asset id for current news article
    And there are no test comments for news
    And created 4 sub comments to 0 root on one level as user 'services'
    And interface language is 'RU'
    And user successfully logs in as confirmed user 'services'
    And user goes to feed Latest
    And the user finds and opens the news article
    When user goes to comments screen
    And user refreshes the screen waiting for comments to appear if there are none
    Then the offset of the list of comments corresponds to the pattern
      | 0 |
      | 1 |
      | 1 |
      | 1 |
    Then user can see Загрузить ещё element
    Then user can not see latest sub comment from index 1
    Examples:
      | index |
      | 20    |

  @nur @android
  Scenario Outline: 'Пользователь видит 4й из 4х подкомментариев нажав "Загрузить еще"'
    Given received a list of Posts
    And define news post for testing = <index>
    And get asset id for current news article
    And there are no test comments for news
    And created 4 sub comments to 0 root on one level as user 'services'
    And interface language is 'RU'
    And user successfully logs in as confirmed user 'services'
    And user goes to feed Latest
    And the user finds and opens the news article
    And user goes to comments screen
    And user refreshes the screen waiting for comments to appear if there are none
    When user tap Загрузить ещё element
    Then the offset of the list of comments corresponds to the pattern
      | 0 |
      | 1 |
      | 1 |
      | 1 |
      | 1 |
    Examples:
      | index |
      | 20    |