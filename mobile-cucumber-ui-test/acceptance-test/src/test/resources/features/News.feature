@t
@news
Feature: News
  As an application user
  I want to see all the titles and news content,
  all reaction lists and percentages,
  number of views of each news,
  how much time has passed since the publication of each news,
  images and captions of images in each news,
  related news at end of each news.

  Background:
    Given application is installed successfully

  @nur @android @titles @contents @tt
  Scenario Outline: 'Проверка заголовков, даты, просмотров, реакций и содержания для выбранных новостей'
    Given received a list of Posts
    And define news post for testing = <index>
    And interface language is 'RU'
    When the user finds and opens the news article
    Then the title inside the body of the news is as expected
    And the publish date inside the body of the news is as expected
    And the views inside the body of the news is as expected
    Then reactions for the news is correct
    And user sees all news content items
    Examples:
      | index |
#      | 0     |
#      | 1     |
#      | 2     |
      | 19    |
#      | 18    |
#      | 17    |

  @nur @android @images
  Scenario Outline: 'Проверка подписей под фото'
    Given received a list of Posts
    And define news post for testing = <index>
    And interface language is 'RU'
    When the user finds and opens the news article
    Then user sees all image captions
    Examples:
      | index |
#      | 0     |
#      | 1     |
#      | 2     |
      | 19    |
      | 18    |
      | 17    |

  @nur @android @share
  Scenario Outline: 'Поделиться новостью через Facebook'
    Given received a list of Posts
    And define news post for testing = <index>
    And interface language is 'RU'
    When the user finds and opens the news article
    When user try to share through Facebook app
    When user logs in into FB account to share
    When user share story
    Then shared story is present in the feed
    Examples:
      | index |
      | 0     |

#  @nur @android @swipe
#  Scenario Outline: 'Проверка возможности перейти к выбранной новости посредством swipe action'

#  @nur @android @related
#  Scenario: 'Related entries'