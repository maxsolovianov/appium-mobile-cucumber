package com.appium.test.model;

import java.util.List;

// single json object
public class PostListResponse {

    // [ ] json objects 0 - 14
    private List<Post> posts;

    public List<Post> getPosts() {
        return this.posts;
    }
}
