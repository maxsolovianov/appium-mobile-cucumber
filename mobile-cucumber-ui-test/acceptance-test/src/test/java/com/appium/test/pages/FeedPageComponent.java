package com.appium.test.pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import java.util.List;

import static com.appium.test.Constants.POST_PLACEHOLDER_LOC;
import static com.appium.test.Constants.ROOT_ITEMS_LOC;
import static com.appium.test.Constants.TITLES_INTO_FEED_LOC;

@Component
@Scope("cucumber-glue")
public class FeedPageComponent extends BasePageComponent {
    public FeedPageComponent(AppiumDriver<? extends MobileElement> driver) {
        super(driver);
    }

    @AndroidFindBy(id = "ivCloseAd")
    @iOSFindBy(id = "id")
    public static MobileElement CLOSE_AD;

    @AndroidFindBy(id = " ")
    @iOSFindBy(accessibility = "NewsFeedRootView.tableView")
    public static MobileElement SCROLLABLE_TABLE_VIEW;

    @AndroidFindBy(id = " ")
    @iOSFindBy(className = "XCUIElementTypeCollectionView")
    public static MobileElement SCROLLABLE_CATEGORIES;

    @AndroidFindAll({@AndroidBy(id = " ")})
    @iOSFindAll({@iOSBy(id = "NewsPostTableViewCell")})
//    @CacheLookup
    public static List<MobileElement> CELL;

    @AndroidFindAll({@AndroidBy(id = " ")})
//    @CacheLookup
    @iOSFindAll({@iOSBy(id = "NewsPostCountOfViewsView.titleLabel")})
    public static List<MobileElement> COUNT_OF_VIEWS;

    @AndroidFindAll({@AndroidBy(id = " ")})
    @iOSFindAll({@iOSBy(id = "NewsPostTableViewCellView.titleLabel")})
//    @CacheLookup
    public static List<MobileElement> TITLE_LABEL;

    @AndroidFindAll({@AndroidBy(id = ROOT_ITEMS_LOC)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> ROOT_ITEMS;

    @AndroidFindAll({@AndroidBy(id = TITLES_INTO_FEED_LOC)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> TITLES;

    @AndroidFindBy(id = POST_PLACEHOLDER_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement POST_PLACEHOLDER;

    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"Проверьте подключение к Интернету.\")")
    @iOSFindBy(id = "id")
    public static MobileElement NO_INTERNET_MESSAGE;

    public NewsPageComponent theUserFindsAndOpensTheNewsArticleByTitle(String title) {
        System.out.println("Поиск новости в ленте по заголовку: " + title);
        int counter = 0;
        while (true) {

            if (isTextPresentIntoMobileElementsOnScreen(TITLES, title)) {
                break;
            }

            else if (elementPresent(NO_INTERNET_MESSAGE)) {
                throw new AssertionError("Проверьте подключение к Интернету. Приложение не может загрузить данные");
            }

            else if (counter == 15) {
                throw new AssertionError("Заголовок новости не был найден в ленте с 15 попытки)");
            }

            swipeUp();
            counter++;
        }
        for (MobileElement el : TITLES) {
            if (el.getText().equalsIgnoreCase(title)) {
                el.click();
                break;
            }
        }

//        // 1 - ждать исчезновения плейсхолдера поста
//        assert elementNotPresent(POST_PLACEHOLDER, 5) :
//                "Ожидание исчезновения элемента POST_PLACEHOLDER 5 сек)";
//
//
//        // 2 - ждать исчезновения плейсхолдеров картинок
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        return new NewsPageComponent(getDriver());
    }
    // .scrollable(true)).scrollIntoView(new UiSelector().textContains(" + '"' + title + '"' + "))"));
    // new UiScrollable(new UiSelector().resourceId("by.tut.nurkz.android:id/rvFeed")).getChildByText(new UiSelector().className("android.widget.TextView"), "Games We Are Playing")));
    // new UiScrollable(new UiSelector().resourceId("by.tut.nurkz.android:id/rvFeed").scrollable(true)).scrollIntoView(new UiSelector().textContains("В ДНР и ЛНР прошли выборы. Их не признают почти нигде"));
}
