package com.appium.test.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Comment {

    public Comment(String assetId, String authorId, String comment, String parentId, int replyCount, List<StatusHistory> statusHistory, List<BodyHistory> bodyHistory) {
        setAsset_id(assetId);
        setAuthor_id(authorId);
        setBody(comment);
        setCreated_at(new Date());
        setUpdated_at(new Date());
        setParent_id(parentId);
        setTags(new ArrayList<>());
        setUser_ip("92.46.69.10");
        setReply_count(replyCount);
        setStatus("ACCEPTED");
        setStatus_history(statusHistory);
        setBody_history(bodyHistory);
    }

    // String
    private String id;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    private Date updated_at;

    private Date created_at;

    private String body;

    private String asset_id;

    // Null
    private String parent_id;

    private String user_ip;

    private String author_id;

    // Array
    private List<String> tags;

    // int32
    private int reply_count;

    private String status;

    // Array
    private List<StatusHistory> status_history;

    // Array
    private List<BodyHistory> body_history;

    public String getAsset_id() {
        return asset_id;
    }

    public void setAsset_id(String asset_id) {
        this.asset_id = asset_id;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getReply_count() {
        return reply_count;
    }

    public void setReply_count(int reply_count) {
        this.reply_count = reply_count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_ip() {
        return user_ip;
    }

    public void setUser_ip(String user_ip) {
        this.user_ip = user_ip;
    }

    public List<BodyHistory> getBody_history() {
        return body_history;
    }

    public void setBody_history(List<BodyHistory> body_history) {
        this.body_history = body_history;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public List<StatusHistory> getStatus_history() {
        return status_history;
    }

    public void setStatus_history(List<StatusHistory> status_history) {
        this.status_history = status_history;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(String author_id) {
        this.author_id = author_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }
}
