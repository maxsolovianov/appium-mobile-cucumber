package com.appium.test.pages;

import com.appium.test.Constants;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static com.appium.test.Constants.*;

@Component
@Scope("cucumber-glue")
public class MenuPageComponent extends BasePageComponent {
    public MenuPageComponent(AppiumDriver<? extends MobileElement> driver) {
        super(driver);
    }

    @AndroidFindBy(id = LOGIN_MENU_BTN_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement LOGIN_BTN;

    @AndroidFindBy(id = Constants.USER_ACCOUNT_NAME)
    @iOSFindBy(id = " ")
    public static MobileElement USER_ACCOUNT_NAME;

    @AndroidFindBy(uiAutomator = MAIN_MENU_ITEM_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement MAIN_MENU_ITEM;

    public LogInPageComponent tapLogIn() {
        LOGIN_BTN.click();
        return new LogInPageComponent(getDriver());
    }

    public FeedPageComponent goToFeedLatest() {
        MAIN_MENU_ITEM.click();
        return new FeedPageComponent(getDriver());
    }

    public boolean isUserAccountNameCorrect(String name) {
        // ждем 30 сек пока завершится запрос и исчезнет progress bar
        assert elementPresent(USER_ACCOUNT_NAME, 10) : "Имя пользователя не появилось в течение 10 сек (\"Приложение не может загрузить данные\")";
        return USER_ACCOUNT_NAME.getText().equalsIgnoreCase(name);
    }
}
