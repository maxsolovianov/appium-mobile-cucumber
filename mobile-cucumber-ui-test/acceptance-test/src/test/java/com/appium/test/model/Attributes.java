package com.appium.test.model;

import com.google.gson.annotations.SerializedName;

public class Attributes {

    @SerializedName("alt")
    private String imageSignature;

    private String src;

    private int width;

    private int height;

    public final String getImageSignature() {
        return this.imageSignature;
    }
}
