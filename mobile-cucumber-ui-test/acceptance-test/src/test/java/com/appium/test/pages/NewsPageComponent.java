package com.appium.test.pages;

import com.appium.test.model.ContentItem;
import com.appium.test.model.Reaction;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import org.joda.time.DateTime;
import org.jsoup.Jsoup;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;

import static com.appium.test.Constants.*;
import static com.support.framework.support.Property.*;

@Component
@Scope("cucumber-glue")
public class NewsPageComponent extends BasePageComponent {
    public NewsPageComponent(AppiumDriver<? extends MobileElement> driver) {
        super(driver);
    }

    private static final Logger LOG = Logger.getLogger(NewsPageComponent.class);

    @AndroidFindBy(id = POST_PLACEHOLDER_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement POST_PLACEHOLDER;

    @AndroidFindBy(xpath = TITLE_INTO_BODY_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement TITLE_INTO_BODY;

    @AndroidFindAll({@AndroidBy(id = CONTENT_LOC)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> TV_CONTENT;

    @AndroidFindAll({@AndroidBy(uiAutomator = IMAGE_CAPTIONS_LOC)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> IMAGE_CAPTIONS;

    @AndroidFindBy(id = REACTION_BTN_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement REACTION_BTN;

    @AndroidFindAll({@AndroidBy(id = REACTION_LABELS_ANDROID_LOC)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> REACTION_LABELS;

    @AndroidFindAll({@AndroidBy(id = REACTION_PERCENTAGES_ANDROID_LOC)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> REACTION_PERCENTAGES;

    @AndroidFindBy(id = TV_NEWS_POST_VIEW_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement TV_NEWS_POST_VIEW;

    @AndroidFindBy(xpath = TV_DATE_INTO_BODY_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement TV_DATE_INTO_BODY;

    @AndroidFindAll({@AndroidBy(xpath = RECOMMENDED_TITLES_ANDROID_LOC)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> RECOMMENDED_TITLES;

    @AndroidFindAll({@AndroidBy(xpath = RECOMMENDED_TIME_ANDROID_LOC)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> RECOMMENDED_TIME;

    @AndroidFindAll({@AndroidBy(xpath = RECOMMENDED_VIEWS_ANDROID_LOC)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> RECOMMENDED_VIEWS;

    @AndroidFindBy(id = STORY_PUBLICATION_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement STORY_PUBLICATION;

    @AndroidFindBy(id = BTN_LIKE_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement BTN_LIKE;

    @AndroidFindBy(xpath = IMAGE_SIGNATURE_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement IMAGE_SIGNATURE;

    @AndroidFindBy(id = SET_COMMENT_BTN_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement SET_COMMENT_BTN;

    @AndroidFindBy(id = "fabShare")
    @iOSFindBy(id = "id")
    public static MobileElement SHARE;

    @AndroidFindBy(id = "bs_main")
    @iOSFindBy(id = "id")
    public static MobileElement SHARE_CONTAINER;

    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"Facebook\")")
    @iOSFindBy(id = "id")
    public static MobileElement FB_ITEM;

    public void userTryToShareThroughFacebookApp() {
        SHARE.click();
        elementPresent(SHARE_CONTAINER, 5);
        elementPresent(FB_ITEM, 5);
        FB_ITEM.click();
//        getDriver().closeApp();
    }

    public boolean isPostPlaceholderNotPresent() {
        return elementNotPresent(POST_PLACEHOLDER, 5);
    }

    public boolean theTitleInsideTheBodyOfTheNewsIsAsExpected(String expected) {
        if (TITLE_INTO_BODY.getText().equalsIgnoreCase(expected)) {
            return true;
        }
        return false;
    }

    public void userSeesAllNewsContentItems(List<ContentItem> items) {
        for (ContentItem item : items) {
            // пропускаем части контента, если это изображение (текста не будет) или если это список (отображается в web view) и т.д.
            if (!item.getTagName().equalsIgnoreCase("p")) {
                continue;
            }
//            String contentItem = splitReturnRussianWordsUntilApostrophe(Jsoup.parse(item.getContent()).body().text());
            String contentItem = Jsoup.parse(item.getContent()).body().text();
            System.out.println("Абзац тела новости...");
            int counter = 0;
            while (true) {
                if (isTextPresentIntoMobileElementsOnScreen(TV_CONTENT, contentItem)) {
                    break;
                } else if (counter == 10) {
                    System.out.println("Элемент не был найден с 10 попытки");
                    assert false;
                    break;
                }
                swipeUp();
                counter++;
            }
        }
    }

    public boolean userSeesAllImageCaptions(List<ContentItem> items) {
        for (ContentItem item : items) {
            // пропускаем части контента, если это НЕ изображение
            if (!item.getTagName().equalsIgnoreCase("img")) {
                continue;
            }
//            String contentItem = splitReturnRussianWordsUntilApostrophe(Jsoup.parse(item.getContent()).body().text());
            String caption = item.getAttributes().getImageSignature();
            if (caption.isEmpty()) { continue; }
            System.out.println("Подпись под изображением...");
            int counter = 0;
            while (true) {
                if (isTextPresentIntoMobileElementsOnScreen(IMAGE_CAPTIONS, caption)) {
                    break;
                } else if (counter == 10) {
                    throw new AssertionError("Элемент подпись не был найден с 10 попытки");
                }
                swipeUp();
                counter++;
            }
        }
        return true;
    }

    public CommentsPageComponent goToCommentsScreen() {
        if (elementClickable(SET_COMMENT_BTN, 5)) {
            SET_COMMENT_BTN.click();
        }
        return new CommentsPageComponent(getDriver());
    }

    public String convertDateFromApiToUi(String pubDate) {
        Date publishDate = new DateTime(pubDate).toDate();
        Locale locale = new Locale("ru");
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM. d, H:mm", locale);
        // временное решение!
        // проверяем, что строка из юая содержит подстроку по урезанному патерну
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d, H:mm", locale);
        if (DEVICE_LOCATION.toString().equalsIgnoreCase("cloud")) {
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        }
        String result = simpleDateFormat.format(publishDate);
        return result;
    }

    // все заменить на java stream API
    public boolean reactionsForTheNewsIsCorrect(List<Reaction> list) {
        REACTION_BTN.click();
        List<String> labelsApi = new ArrayList<>();
        List<String> labelsUi = new ArrayList<>();
        REACTION_LABELS.forEach(element -> labelsUi.add(element.getText()));
        list.forEach(reaction -> labelsApi.add(reaction.getLabel()));
        List<Integer> percApi = new ArrayList<>();
        List<Integer> percUi = new ArrayList<>();
        REACTION_PERCENTAGES.forEach(element -> percUi.add(Integer.parseInt(element.getText().replaceAll("[^0-9]", ""))));
        list.forEach(reaction -> percApi.add(reaction.getValue()));
        int total = percApi.stream().mapToInt(i -> i).sum();
        List<Integer> l = new ArrayList<>();
        percApi.forEach(i -> l.add(i * 100 / total));

        System.out.println(labelsApi.toString());
        System.out.println(labelsUi.toString());
        System.out.println(l.toString());
        System.out.println(percUi.toString());

        return labelsApi.equals(labelsUi) && l.equals(percUi);
    }
}
