package com.appium.test.stepdefs;

import com.appium.test.pages.LogInPageComponent;
import com.appium.test.pages.MenuPageComponent;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.springframework.beans.factory.annotation.Autowired;

import static com.appium.test.Constants.ERROR_MESSAGE_EXPECTED_TEXT;

public class MenuStepDefs {

    @Autowired
    private MenuPageComponent menuPageComponent;

    @Autowired
    private LogInPageComponent logInPageComponent;

    @Then("^user successfully logs in as confirmed user '(.*)'$")
    public void userSuccessfullyLogsInAs(String account) {
        menuPageComponent.goToSidebarMenu().tapLogIn().logInAsUser(account);

        assert menuPageComponent.isUserAccountNameCorrect(account) :
                "Имя пользователя на панели меню не соответствует ожидаемому";
    }

    @Then("^unverified user '(.*)' cannot log in$")
    public void unverifiedUser(String account) {
        menuPageComponent.goToSidebarMenu().tapLogIn().unsuccessfullyLogInAsUser(account);
        assert logInPageComponent.userSeesAnErrorMessage() :
                "Сообщение об ошибке: " + ERROR_MESSAGE_EXPECTED_TEXT + " не было отображено в течение 5 сек";
    }

    @And("^user goes to feed Latest$")
    public void userGoesToLatestPage() {
        menuPageComponent.goToFeedLatest();
    }
}
