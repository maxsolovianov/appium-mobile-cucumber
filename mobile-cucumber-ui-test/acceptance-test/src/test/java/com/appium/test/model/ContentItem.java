package com.appium.test.model;

public class ContentItem {

    private String content;

    private String tagName; // list - отображается в web view

    private Attributes attributes;

    public final String getContent() {
        return this.content;
    }

    public final String getTagName() {
        return this.tagName;
    }

    public final Attributes getAttributes() {
        return this.attributes;
    }
}
