package com.appium.test.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class FacebookGraphApi {
    private static final String FACEBOOK_APP_ID = "1407652382866683";
    private static final String FACEBOOK_APP_SECRET = "d264556764d07762b9838f783481bc6f";
    private static final String GRANT_TYPE = "client_credentials";
    private static final String FB_GRAPH_API_HOST = "https://graph.facebook.com";
    private static final String OAUTH_ACCESS_TOKEN_ENDPOINT = "/oauth/access_token";
    private static final String TEST_USERS_ENDPOINT = "/accounts/test-users";
    private static final String API_VERSION = "v3.0";
    private static final String INSTALLED = "true";
    private static final String NAME = "nvieniue";
    private static final String LOCALE = "en_US";
    private static final String PERMISSIONS = "read_stream,user_posts";
    private static final String METHOD = "post";
    private static final String FIELDS = "feed.limit(1)";

    public static String getApplicationAccessToken() {
        Map<String, String> params = new HashMap<>();
        params.put("client_id", FACEBOOK_APP_ID);
        params.put("client_secret", FACEBOOK_APP_SECRET);
        params.put("grant_type", GRANT_TYPE);
        String text = given().params(params).get(FB_GRAPH_API_HOST + OAUTH_ACCESS_TOKEN_ENDPOINT).path("access_token");
        System.out.println("Application access token: " + text);
        return text;
    }

    private Map convertJsonStringToHashMap(String jsonString) throws IOException {
        System.out.println("Fb test account as string: " + jsonString);
        HashMap<String, Object> map = new ObjectMapper().readValue(jsonString, HashMap.class);
        System.out.println("Test user id " + map.get("id").toString());
        System.out.println("Test user token " + map.get("access_token").toString());
        System.out.println("Test user email " + map.get("email").toString());
        System.out.println("Test user password " + map.get("password").toString());
        return map;
    }

    public Map getTestAccount(String appAccessToken) throws IOException {
        Map<String, String> params = new HashMap<>();
        params.put("installed", INSTALLED);
        params.put("name", NAME);
        params.put("locale", LOCALE);
        params.put("permissions", PERMISSIONS);
        params.put("method", METHOD);
        params.put("access_token", appAccessToken);
        given().params(params).get(FB_GRAPH_API_HOST + "/" + FACEBOOK_APP_ID + TEST_USERS_ENDPOINT).getBody().asString();
        return convertJsonStringToHashMap(given().params(params).get(FB_GRAPH_API_HOST + "/" + FACEBOOK_APP_ID + TEST_USERS_ENDPOINT).getBody().asString());
    }

    // удаляем все тестовые аккаунты
//    public static void main(String[] args) throws InterruptedException {
//        Map<String, String> params = new HashMap<>();
//        params.put("access_token", getApplicationAccessToken());
//        Response response = given().params(params).get("https://graph.facebook.com/v3.2/1407652382866683/accounts/test-users/");
//        System.out.println(response.body().prettyPrint());
//        Data[] datas = response.jsonPath().getObject("data", Data[].class);
//        Map<String, String> testUser = new HashMap<>();
//        for (Data data : datas) {
//            String id = data.id;
//            testUser.put("access_token", data.access_token);
//            System.out.println("Remove test account: " + given().params(testUser).delete(FB_GRAPH_API_HOST + "/" + API_VERSION + "/" + id).getBody().asString());
//            Thread.sleep(3000);
//        }
//    }

    public void removeTestAccount(Map testUser) {
        // айди юзера, токен юзера
        // curl -X DELETE https://graph.facebook.com/v3.0/100791270825664?access_token=EAAUAQJwZCfPsBACWGkudTLHdA0naABNFzX6rRGZCvfs2FfSZBiT1SMAHxZBBVAw769s2n4BfLNYYdwV04i4chirsT8jYL6r3pyRT77nUQl0m2evPvV73KuVukbH8bO5dgwYubFQAGwCwS6sQmlBp2EFa4hv9CpZCFvgLx2vtYlGZAZBZAdFUXuf7v423FQYByjZCMGh9Y7PzZBuyVwt6L5KRV3
        Map<String, String> params = new HashMap<>();
        params.put("access_token", testUser.get("access_token").toString());
        System.out.println("Remove test account: " + given().params(params).delete(FB_GRAPH_API_HOST + "/" + API_VERSION + "/" + testUser.get("id")).getBody().asString());
    }

    public Map getLatestObjectFromTestUserFeed(Map testUser) {
        Map<String, String> params = new HashMap<>();
        params.put("fields", FIELDS);
        params.put("access_token", testUser.get("access_token").toString());
        ResponseBody body = given().params(params).get(FB_GRAPH_API_HOST + "/" + API_VERSION + "/" + testUser.get("id").toString()).getBody();
        JsonPath path = new JsonPath(body.asString());
        return path.getMap("feed.data[0]");
    }

    public static class Data {
        String id;
        String login_url;
        String access_token;

        public String getId() {
            return id;
        }

        public String getLogin_url() {
            return login_url;
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setLogin_url(String login_url) {
            this.login_url = login_url;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }
    }
}
