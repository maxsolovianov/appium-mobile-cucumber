package com.appium.test.utils;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import org.bson.BsonNull;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.types.ObjectId;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static com.appium.test.Constants.MONGO_URI;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

@Component
@Scope("cucumber-glue")
public class MongoUtils {

    MongoClientURI uri = new MongoClientURI(MONGO_URI);
    MongoClient mongoClient = new MongoClient(uri);
    MongoDatabase db = mongoClient.getDatabase("talk");
    CodecRegistry pojoCodecRegistry =
            fromRegistries(MongoClient.getDefaultCodecRegistry(),
                    fromProviders(PojoCodecProvider.builder().automatic(true).build()));
    MongoCollection<Comment> commentMongoCollection = db.getCollection("comments", Comment.class).withCodecRegistry(pojoCodecRegistry);

    /**
     * @param number     - колличество комментариев (верхнего уровня или к рутовому)
     * @param assetId    - идентификатор статьи
     * @param authorId   - идентификатор автора
     * @param parentId   - айди рутового комментария или null, если верхнего уровня
     * @param replyCount - колличество ответов (если предполагаются подкомментарии)
     * @return - список айдишек (поля id, body объекта) для поиска через UI
     */
    public List<String> insertComments(int number, String assetId, String authorId, String parentId, int replyCount) {

        List<String> commentsIds = new ArrayList<>();
        for (int x = 0; x < number; ++x) {
            commentsIds.add(UUID.randomUUID().toString());
        }

        for (String id : commentsIds) {

            BodyHistory bodyHistory = new BodyHistory();
            bodyHistory.setId(new ObjectId());
            bodyHistory.setBody(id);
            bodyHistory.setCreated_at(new Date());
            List<BodyHistory> bodyHistoryList = new ArrayList<>();
            bodyHistoryList.add(bodyHistory);

            StatusHistory statusHistory = new StatusHistory();
            statusHistory.setType("NONE");
            statusHistory.setCreated_at(new Date());
            statusHistory.setAssigned_by(BsonNull.VALUE);
            List<StatusHistory> statusHistoryList = new ArrayList<>();
            statusHistoryList.add(statusHistory);

            Comment comment = new Comment(assetId, authorId, id, parentId, replyCount, statusHistoryList, bodyHistoryList);
            commentMongoCollection.insertOne(comment);
            commentMongoCollection.updateOne(eq("body", id), combine(set("id", id)));
            commentMongoCollection.updateOne(eq("body", id), combine(set("data-item-marker", "test data")));
        }
        return commentsIds;
    }

    public String getUserId(String username) {
        Document user = db.getCollection("users").find(eq("username", username)).first();
        return user.get("id").toString();
    }

    public void clearAllTestDataFromCommentsCollection(String assetId) {
        DeleteResult deleteResult = commentMongoCollection.deleteMany(eq("asset_id", assetId));
        System.out.println("deleted before test execution: " + deleteResult.getDeletedCount());
    }

//    public static void main(String[] args) {
//        MongoUtils mongoUtils = new MongoUtils();
//        String userId = mongoUtils.getUserId("services");
//        String assetId = mongoUtils.getNewsAssetId("NUR.KZ обращается к владельцам iPhone");
//        // создаем 4 корневых
//        List<String> ids = mongoUtils.insertComments(1, assetId, userId, null, 4);
//        // создаем 4 подкомментария к 0 корневому
//        String firstRootComment = ids.get(0);
//        mongoUtils.insertComments(4, assetId, userId, firstRootComment, 0);
//    }
}
