package com.appium.test.model;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import io.restassured.response.Response;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.support.framework.support.Property.*;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.post;

class NurAssetIdRequestBody {

    public String query = "query GetAsset($assetID: String) {\n" +
            "  asset (url: $assetID) {\n" +
            "    id\n" +
            "  }\n" +
            "}";
    public Variables variables = new Variables();

    public NurAssetIdRequestBody(String url) {
        variables.assetID = url;
    }

    public class Variables {
        String assetID;
    }
}

public class APIRequest {

    private static final Logger LOG = Logger.getLogger(APIRequest.class);

    public static Map getParameters(String language, String category) {
        Map<String, String> params = new HashMap<>();
        params.put("category", category);
        params.put("per-page", "50");
        params.put("short", "0");
        params.put("offsetPostId", "0");
        params.put("showTrending", "1");
        params.put("lang", language);
        return params;
    }

    public static PostListResponse getPostList(String language, String category) {
        String json = given().params(getParameters(language, category)).get(MOBILE_API_HOST.toString() + NEWS_LIST_ENDPOINT.toString()).getBody().asString();
//        String json = given().params(getParameters(language, category)).get("https://app.nur.stage.systools.pro/v3/post/list").getBody().asString();
        return new Gson().fromJson(json, PostListResponse.class);
    }

    /**
     * возвращает отсортированный список объектов типа  Post
     * трендинговые - вверху списка
     * @return
     */
    public static List<Post> getOrderedPosts(String language, String category) {
        PostListResponse postListResponse = getPostList(language, category);
        List<Post> posts = postListResponse.getPosts();
        List<Post> trendingPostList = new ArrayList<>();
        List<Post> otherPostList = new ArrayList<>();
        for (Post post : posts) {
            if (post.isTrending()) {
                trendingPostList.add(0, post);
            } else if (!post.isTrending()) {
                otherPostList.add(post);
            }
        }
        List<Post> reversedTrendingPostList = Lists.reverse(trendingPostList);
        List<Post> newList = new ArrayList<>(reversedTrendingPostList);
        newList.addAll(otherPostList);
//        System.out.println("Отсортированный список заголовков всех постов: ");
//        for (Post post : newList) {
//            System.out.println(post.getTitle());
//        }
        return newList;
    }

//    public static void main(String[] args) {
//        List<Post> currentListPosts = getOrderedPosts(getPostList("ru", "latest"));
//        for (Post post : currentListPosts) {
//            System.out.println(post.getTitle());
//        }
//    }

    public static Category[] getCategoriesArray() {
        String json = given().get(MOBILE_API_HOST.toString() + CATEGORIES_ENDPOINT.toString()).getBody().asString();
        return new Gson().fromJson(json, Category[].class);
    }

    public static String getAssetId(String publicUrl) {
        NurAssetIdRequestBody nurAssetIdRequestBody = new NurAssetIdRequestBody(publicUrl);
        Gson gson = new Gson();
        String json = gson.toJson(nurAssetIdRequestBody, NurAssetIdRequestBody.class);
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        Response resp = given().body(json).headers(headers).post("https://comments.nur.stage.systools.pro/api/v1/graph/ql");
        String assetId = resp.jsonPath().getString("data.asset.id");
        System.out.println("asset id for this news index: " + assetId);
        return assetId;
    }
}
