package com.appium.test.helpers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.Map;

public class JsonArrayToPojoArray {
    public static final String URI = "https://app.nur.stage.systools.pro/v3/post/list";
    public static void main(String[] args) {
        RestAssured.baseURI = URI;
        RequestSpecification request = RestAssured.given();
        Map<String, String> params = new HashMap<>();
        params.put("category", "latest");
        params.put("per-page", "15");
        params.put("short", "0");
        params.put("offsetPostId", "0");
        params.put("showTrending", "1");
        params.put("lang", "ru");
        request.params(params);
        Response response = request.get();
//        System.out.println(response.statusCode());
        Post[] posts;
        posts = response.jsonPath().getObject("currentListPosts", Post[].class);
//        System.out.println(currentListPosts.length);
        for (Post post : posts) {
            System.out.println(post.getTitle());
            System.out.println(post.getId());
        }
    }
}

// it class-level annotation will ignore every property you haven't defined in your POJO
@JsonIgnoreProperties(ignoreUnknown = true)
class Post {
    private String id;
    private String title;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
