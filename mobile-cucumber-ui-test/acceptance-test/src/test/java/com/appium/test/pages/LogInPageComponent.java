package com.appium.test.pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.appium.test.Constants.*;

@Component
@Scope("cucumber-glue")
public class LogInPageComponent extends BasePageComponent {
    public LogInPageComponent(AppiumDriver<? extends MobileElement> driver) {
        super(driver);
    }

    @AndroidFindAll({@AndroidBy(id = TEXT_FIELDS_PANEL)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> TEXT_FIELDS;

    @AndroidFindBy(id = PHONE_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement PHONE;

    @AndroidFindBy(id = PASSWORD_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement PASSWORD;

    @AndroidFindBy(id = LOGIN_BTN_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement LOGIN_BTN;

    @AndroidFindBy(id = RESTORE_PASSWORD_LINK_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement RESTORE_PASSWORD_LINK;

    @AndroidFindBy(id = REGISTRATION_LINK_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement REGISTRATION_LINK;

    @AndroidFindBy(id = NAVIGATE_UP_BTN_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement NAVIGATE_UP_BTN;

    @AndroidFindBy(id = ERROR_MESSAGE_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement ERROR_MESSAGE;

    @AndroidFindBy(className = PROGRESS_BAR_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement PROGRESS_BAR;

    private void typePhone(String phone) {
        TEXT_FIELDS.get(0).setValue(phone);
    }

    private void typePassword(String password) {
        TEXT_FIELDS.get(1).setValue(password);
    }

    private void tapLogIn() {
        LOGIN_BTN.click();
    }

    private Map getPhoneAndPasswordForAccount(String account) {
        Map dict = new HashMap<String, String>();
        switch (account) {
            case "services":
                dict.put("phone", SERVICES_PHONE);
                dict.put("password", SERVICES_PASSWORD);
                break;
            case "Developer1":
                dict.put("phone", DEVELOPER1_PHONE);
                dict.put("password", DEVELOPER1_PASSWORD);
                break;
            case "Developer2":
                dict.put("phone", DEVELOPER2_PHONE);
                dict.put("password", DEVELOPER2_PASSWORD);
                break;
        }
        return dict;
    }

    public MenuPageComponent logInAsUser(String account) {
        typePhone(getPhoneAndPasswordForAccount(account).get("phone").toString());
        typePassword(getPhoneAndPasswordForAccount(account).get("password").toString());
        tapLogIn();
        // https://www.browserstack.com/s3-debug/testautomation/eef035c33079d0443f790953dc93da17563d1bd5/screenshot-8ddba3517e.jpeg
//        assert elementNotPresent(PROGRESS_BAR, 5) : "Элемент PROGRESS_BAR не исчез в течение 5 сек";
        return new MenuPageComponent(getDriver());
    }

    public LogInPageComponent unsuccessfullyLogInAsUser(String account) {
        typePhone(getPhoneAndPasswordForAccount(account).get("phone").toString());
        typePassword(getPhoneAndPasswordForAccount(account).get("password").toString());
        tapLogIn();
//        assert elementNotPresent(PROGRESS_BAR, 5) : "Элемент PROGRESS_BAR не исчез в течение 5 сек";
        return new LogInPageComponent(getDriver());
    }

    public boolean userSeesAnErrorMessage() {
        return elementAttributeContains(ERROR_MESSAGE, "text", ERROR_MESSAGE_EXPECTED_TEXT, 5);
    }
}
