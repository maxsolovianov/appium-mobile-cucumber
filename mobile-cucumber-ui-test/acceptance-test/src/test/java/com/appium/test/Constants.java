package com.appium.test;

public class Constants {
    // Credentials
    public static final String SERVICES_PHONE = "111111111";
    public static final String SERVICES_PASSWORD = "DulU0CPGVr3TQa0y31co";
    public static final String DEVELOPER1_PHONE = "222222222";
    public static final String DEVELOPER1_PASSWORD = "qwert";
    public static final String DEVELOPER2_PHONE = "444444444";
    public static final String DEVELOPER2_PASSWORD = "qwert";
    //    Mongo
    public static final String MONGO_URI = "mongodb://talkadm:ZxPhda2p5zXkY3ZPwwnz@qa1.hw.systools.pro:3100/?authSource=talk";
    //    FeedPageComponent
    public static final String FEED_PLACEHOLDER = "by.tut.nurkz.android:id/feedPlaceholder";
    public static final String ROOT_ITEMS_LOC = "root_item";
    public static final String TITLES_INTO_FEED_LOC = "tv_title";
    //    NewsPageComponent
    public static final String POST_PLACEHOLDER_LOC = "by.tut.nurkz.android:id/postPlaceholder";
    public static final String IMAGE_PLACEHOLDER_LOC = "";
    public static final String IMAGE_CAPTIONS_LOC = "new UiSelector().resourceId(\"by.tut.nurkz.android:id/rvPostItems\").childSelector(new UiSelector().className(\"android.widget.RelativeLayout\")).childSelector(new UiSelector().resourceId(\"by.tut.nurkz.android:id/tv_title\"))";
    public static final String TITLE_INTO_BODY_ANDROID_LOC = "//android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView[contains(@resource-id, 'tv_title')]";
    public static final String REACTION_BTN_ANDROID_LOC = "tvReaction";
    public static final String CONTENT_LOC = "tv_content";
    public static final String REACTION_LABELS_ANDROID_LOC = "tvReactionLabel";
    public static final String REACTION_PERCENTAGES_ANDROID_LOC = "tvReactionPercentage";
    public static final String TV_NEWS_POST_VIEW_ANDROID_LOC = "tv_post_views";
    public static final String TV_DATE_INTO_BODY_ANDROID_LOC = "//android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView[contains(@resource-id, 'tv_date')]";
    public static final String RECOMMENDED_TITLES_ANDROID_LOC = "//android.widget.LinearLayout[attribute::resource-id='by.tut.nurkz.android:id/ll_item_recommended_posts']//android.widget.TextView[attribute::resource-id='by.tut.nurkz.android:id/tv_title']";
    public static final String RECOMMENDED_TIME_ANDROID_LOC = "//android.widget.LinearLayout[attribute::resource-id='by.tut.nurkz.android:id/ll_item_recommended_posts']//android.widget.TextView[attribute::resource-id='by.tut.nurkz.android:id/tvTime']";
    public static final String RECOMMENDED_VIEWS_ANDROID_LOC = "//android.widget.LinearLayout[attribute::resource-id='by.tut.nurkz.android:id/ll_item_recommended_posts']//android.widget.TextView[attribute::resource-id='by.tut.nurkz.android:id/tv_views_count']";
    public static final String STORY_PUBLICATION_ANDROID_LOC = "llReactions";
    public static final String BTN_LIKE_ANDROID_LOC = "btn_like";
    public static final String IMAGE_SIGNATURE_ANDROID_LOC = "//android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[attribute::resource-id='by.tut.nurkz.android:id/tv_title']";
    public static final String SET_COMMENT_BTN_ANDROID_LOC = "iv_comment";
    //    LogInPageComponent
    public static final String PROGRESS_BAR_LOC = "android.widget.ProgressBar"; // loading overlay
    public static final String ERROR_TEXT_LOC = "by.tut.nurkz.android:id/snackbar_text"; // Что-то пошло не так. Попробуйте, пожалуйста, позже!
    public static final String PHONE_ANDROID_LOC = "tfbPhoneContainer";
    public static final String PASSWORD_ANDROID_LOC = "tfbPasswordContainer";
    public static final String TEXT_FIELDS_PANEL = "text_field_boxes_upper_panel";
    public static final String LOGIN_BTN_ANDROID_LOC = "btLogin";
    public static final String RESTORE_PASSWORD_LINK_ANDROID_LOC = "tvForgotPassword_AA";
    public static final String REGISTRATION_LINK_ANDROID_LOC = "tvNoAccount";
    public static final String NAVIGATE_UP_BTN_ANDROID_LOC = "Navigate up";
    public static final String ERROR_MESSAGE_ANDROID_LOC = "text_field_boxes_helper";
    public static final String ERROR_MESSAGE_EXPECTED_TEXT = "Такого логина или пароля не существует. Проверьте правильность ввода";
    //    MenuPageComponent
    public static final String MAIN_MENU_ITEM_LOC = "new UiSelector().text(\"Главная\")";
    public static final String LOGIN_MENU_BTN_ANDROID_LOC = "btnLogin";
    public static final String USER_ACCOUNT_NAME = "tvDrawerUserName";
    public static final String MENU_ITEMS_ANDROID_LOC = "design_menu_item_text"; //
    //    CommentsPageComponent
    public static final String NO_COMMENTS_ON_THE_ARTICLE_LOC = "new UiSelector().text(\"Нет комментариев к статье\")";
    public static final String LOAD_MORE_ANDROID_LOC = "tv_load_more"; // Загрузить ещё
    public static final String SET_COMMENT_FIELD_ANDROID_LOC = "etComment";
    public static final String SEND_COMMENT_BTN_ANDROID_LOC = "btnCommentSend";
    public static final String COMMENTS_CONTENT_ANDROID_LOC = "tv_comment_content";
    public static final String COMMENTS_DATE_ANDROID_LOC = "tv_comment_date";
    public static final String MODERATION_STATUS_MESSAGE = "На модерации";
    public static final String COMMENT_CONTAINERS_ANDROID_LOC = "ll_comment_container";
    public static final String COMMENTS_ANSWER_BTN_ANDROID_LOC = "tv_comments_answer_btn"; // Ответить
    public static final String REPLY_INFO_LOC = "tvReplyInfo";
    public static final String COMMENT_AUTHORS_LOC = "tv_comment_author";
}
