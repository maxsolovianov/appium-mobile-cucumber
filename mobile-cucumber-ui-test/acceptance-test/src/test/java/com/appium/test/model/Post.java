package com.appium.test.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Post {

    private String title;

    private String publishDate;

    private int views;

    private boolean isTrending;

    public boolean isTrending() {
        return isTrending;
    }

    // [ ] json objects
    @SerializedName("content")
    private List<ContentItem> contentList;

    // [ ] json objects
    @SerializedName("reactions")
    private List<Reaction> reactionList;

    private String url;

    public final String getTitle() {
        return this.title;
    }

    public final String getPublishDate() {
        return this.publishDate;
    }

    public final int getViews() {
        return this.views;
    }

    public final List<ContentItem> getContentItem() {
        return this.contentList;
    }

    public final List<Reaction> getReactionList() {
        return this.reactionList;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
