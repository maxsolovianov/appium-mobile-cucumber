package com.appium.test.pages;

import com.appium.framework.base.AppiumBase;
import com.appium.test.model.APIRequest;
import com.appium.test.model.Post;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.regex.Pattern;

@Component
@Scope("cucumber-glue")
public class BasePageComponent extends AppiumBase {
    public BasePageComponent(AppiumDriver<? extends MobileElement> driver) {
        super(driver);
    }

    public boolean isTextPresentIntoMobileElementsOnScreen(List<MobileElement> elements, String expected) {
        System.out.println("Ищется:  " + expected);
        boolean isPresent = false;
        for (MobileElement el : elements) {
            if (el.getText().replaceAll("\\s+$", "").equalsIgnoreCase(expected)) {
                System.out.println("Найдено: " + el.getText());
                isPresent = true;
                break;
            }
        }
        return isPresent;
    }

    public void pullToRefreshScreen() {
        swipeDown();
    }

    public MenuPageComponent goToSidebarMenu() {
        swipeRight();
        return new MenuPageComponent(getDriver());
    }

    public List<Post> receivedAListOfPosts() {
        return APIRequest.getOrderedPosts("ru", "latest");
    }

    public String getAssetIdForCurrentNewsArticle(String url) {
        return APIRequest.getAssetId(url);
    }

    // ------------------------------------------------------------------

    public MobileElement getTextViewWhichContainsText(String text) {
        return getElementWithClassWhichContainsText("android.widget.TextView", text);
    }

    public MobileElement getElementWithClassWhichContainsText(String className, String text) {
        return getDriver().findElement(By.xpath("//" + className + "[contains(@text, '" + text + "')]"));
    }

    public WebElement moveMouseAndSendKeys(WebElement element, String charSequence) {
        Actions actions = new Actions(getDriver());
        // Move mouse
        actions.moveToElement(element);
        actions.click();
        // Send a sequence of key to the active element
        actions.sendKeys(charSequence);
        actions.build().perform();
        return element;
    }

    public String splitReturnRussianWordsUntilApostrophe
            (String input) {
        String output = null;
        if (input.contains("'")) {
            // возвращает только русскую подстроку, если в англ есть '
            Pattern pattern = Pattern.compile("(?=.*\\w)(\\w|')+");
            java.util.regex.Matcher matcher = pattern.matcher(input);
            if (matcher.find()) {
                output = input.split("(?=.*\\w)(\\w|')+")[0];
            }
            return output;
        } else {
            return input;
        }
    }
}
