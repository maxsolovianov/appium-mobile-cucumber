package com.appium.test.stepdefs;

import com.appium.framework.base.AppiumBase;
import com.appium.test.pages.BasePageComponent;
import com.appium.test.pages.CommonData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertTrue;

public class BaseStepDefs {

    @Autowired
    private AppiumBase appiumBase;

    @Autowired
    private BasePageComponent basePageComponent;

    @Autowired
    private CommonData commonData;

    @Given("^application is installed successfully$")
    public void isAppInstalled() {
        String androidBundleIdentifier = "by.tut.nurkz.android";
        assert appiumBase.getDriver().isAppInstalled(androidBundleIdentifier) :
                "Приложение: " + androidBundleIdentifier + " не установлено на устройстве";
    }

    @Given("^application is removed successfully$")
    public void isAppRemoved() {
        assertTrue(appiumBase.getDriver().removeApp("by.tut.nurkz.android"));
    }

    @When("^pull to refresh$")
    public void pullToRefresh() {
        basePageComponent.pullToRefreshScreen();
    }

    @Given("^received a list of Posts$")
    public void receivedAListOfPosts() {
        commonData.currentListPosts = basePageComponent.receivedAListOfPosts();
    }

    @Given("^define news post for testing = (.*)$")
    public void defineNewsForTesting(int index) {
        commonData.currentTestPost = commonData.currentListPosts.get(index);
    }

    @Given("^get asset id for current news article$")
    public void getAssetIdForCurrentNewsArticle() {
        commonData.currentAssetId = basePageComponent.getAssetIdForCurrentNewsArticle(commonData.currentTestPost.getUrl());
    }
}
