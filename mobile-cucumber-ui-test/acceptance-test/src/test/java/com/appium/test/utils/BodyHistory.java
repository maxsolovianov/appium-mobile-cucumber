package com.appium.test.utils;

import org.bson.types.ObjectId;

import java.util.Date;

public class BodyHistory {

    private ObjectId _id;

    private String body;

    private Date created_at;

    public ObjectId getId() {
        return _id;
    }

    public void setId(ObjectId id) {
        this._id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }
}
