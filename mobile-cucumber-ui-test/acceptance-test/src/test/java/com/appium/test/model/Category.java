package com.appium.test.model;

public class Category {

    private int id;

    private String name;

    private String slug;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
