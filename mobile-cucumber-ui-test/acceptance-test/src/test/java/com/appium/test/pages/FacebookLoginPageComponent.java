package com.appium.test.pages;

import com.appium.test.helpers.FacebookGraphApi;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Map;

@Component
@Scope("cucumber-glue")
public class FacebookLoginPageComponent extends BasePageComponent {
    public FacebookLoginPageComponent(AppiumDriver<? extends MobileElement> driver) {
        super(driver);
    }

    // ------------ Native Facebook Application  -------------------------

    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"Phone\")")
    @iOSFindBy(id = "id")
    public static MobileElement PHONE_NUMBER_OR_EMAIL_ADDRESS;

    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"Password\")")
    @iOSFindBy(id = "id")
    public static MobileElement PASSWORD;

    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"LOG IN\")")
    @iOSFindBy(id = "id")
    public static MobileElement LOG_IN;

    // new UiSelector().textContains("Write")
    @AndroidFindBy(className = "android.widget.EditText")
    @iOSFindBy(id = "id")
    public static MobileElement WRITE_SOMETHING;
    // new UiSelector().className("android.widget.Button")
    @AndroidFindBy(className = "android.widget.Button")
    @iOSFindBy(id = "id")
    public static MobileElement POST;

    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"TEST 111\")")
    @iOSFindBy(id = "id")
    public static MobileElement CURRENT_MESSAGE;

    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"DONE\")")
    @iOSFindBy(id = "id")
    public static MobileElement DONE;

    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"CONTINUE\")")
    @iOSFindBy(id = "id")
    public static MobileElement CONTINUE;

    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"Sending...\")")
    @iOSFindBy(id = "id")
    public static MobileElement SENDING;

    @AndroidFindBy(id = "fabShare")
    @iOSFindBy(id = "id")
    public static MobileElement SHARE;

    // ------------ Web View from Facebook SDK --------------------------

    @FindBy(id = "m_login_email")
    @iOSBy(id = "id")
    public static MobileElement FACEBOOK_LOGIN;

    @FindBy(id = "m_login_password")
    @iOSFindBy(id = "id")
    public static MobileElement FACEBOOK_PASSWORD;

    @FindBy(id = "u_0_6")
    @iOSFindBy(id = "id")
    public static MobileElement FACEBOOK_LOG_IN_BTN;

    @FindBy(xpath = "//textarea")
    @iOSFindBy(id = "id")
    public static MobileElement TEXT_AREA;

    @FindBy(xpath = "//*[@value='Post']")
    @iOSFindBy(id = "id")
    public static MobileElement PUBLISH_THIS;

    @FindBy(xpath = "//*[@value='TEST 111']")
    @iOSFindBy(id = "id")
    public static MobileElement TYPED_MESSAGE;

    private static final String TEST_MESSAGE = "TEST 111";

    // это поле будет инициализировано при создании бина в контейнере
    Map<String, String> testAccountMap = null;

    FacebookGraphApi facebookGraphApi = new FacebookGraphApi();

    public String getTestMessage() {
        return TEST_MESSAGE;
    }

    public boolean createTestAccount() {
        try {
            testAccountMap = facebookGraphApi.getTestAccount(facebookGraphApi.getApplicationAccessToken());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void acceptPrivacyPolicy() {
        // native alert Terms and Privacy Policy
        if (elementPresent(CONTINUE)) {
            CONTINUE.click();
        }
    }

    public void typeUserName() {
        elementPresent(PHONE_NUMBER_OR_EMAIL_ADDRESS, 10);
        PHONE_NUMBER_OR_EMAIL_ADDRESS.sendKeys(testAccountMap.get("email"));
    }

    public void typePassword() {
        PASSWORD.sendKeys(testAccountMap.get("password"));
    }

    public void submitLogin() {
        LOG_IN.click();
    }

    public void typeMessageToFeedWithPost() {
        assert elementPresent(WRITE_SOMETHING, 10) : "Элемент WRITE_SOMETHING не найден в течение 10 сек";
        WRITE_SOMETHING.sendKeys(TEST_MESSAGE);
    }

    public void checkTypedMessage() {
        assertElementPresent(CURRENT_MESSAGE, 5);
    }

    public void publishPost() {

        assert elementPresent(POST, 5) : "Элемент POST не был найден в течение 5 сек";
       POST.click();
    }

    // проблема FB приложения(?) на Galaxy S9
    public void elementSendingPresent() {
        assert elementPresent(SENDING, 5) : "Элемент SENDING не был найден в течение 5 сек";
    }

    public String getLastPostMessage() {
        String message;
        int index = 0;
        do {
            try {
                message = facebookGraphApi.getLatestObjectFromTestUserFeed(testAccountMap).get("message").toString();
                System.out.println("returned message from Facebook API: " + message);
            } catch (java.lang.NullPointerException ex) {
                System.out.println("NullPointerException!!!");
                message = null;
            }
            ++index;

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (index == 10) {
                break;
            }
        }
        while (message == null);
        return message;
    }

    public void removeTestAccount() {
        facebookGraphApi.removeTestAccount(testAccountMap);
    }

    public void checkThatMessageInFeedMatchesTheExpected() {
        assertEquals(getTestMessage(), getLastPostMessage());
    }
}
