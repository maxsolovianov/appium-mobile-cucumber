package com.appium.test.pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.junit.Assert.assertEquals;

@Component
@Scope("cucumber-glue")
public class LangPageComponent extends BasePageComponent {
    public LangPageComponent(AppiumDriver<? extends MobileElement> driver) {
        super(driver);
    }

    @AndroidFindBy(id = "by.tut.nurkz.android:id/rbLangRu")
    @iOSFindBy(id = "id")
    public static MobileElement RUSSIAN_LANG_ITEM;

    @AndroidFindBy(id = "by.tut.nurkz.android:id/rbLangKz")
    @iOSFindBy(id = "id")
    public static MobileElement KZ_LANG_ITEM;

    @AndroidFindBy(id = "by.tut.nurkz.android:id/tvSaveChanges")
    @iOSFindBy(id = "id")
    public static MobileElement LANG_OK;

    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"Wait\")")
    @iOSFindBy(id = "id")
    public static MobileElement WAIT_BROWSERSTACK;

    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"BrowserStack USB isn't responding\")")
    @iOSFindBy(id = "id")
    public static MobileElement BROWSERSTACK_USB_IS_NOT_RESPONDING;

    public void selectInterfaceLanguage(String language) {
        switch (language) {
            case "RU" :
                RUSSIAN_LANG_ITEM.click();
                break;
            case "KZ" :
                KZ_LANG_ITEM.click();
                break;
        }
        LANG_OK.click();
    }
}
