package com.appium.test.pages;

import com.google.common.collect.Lists;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.appium.test.Constants.*;

@Component
@Scope("cucumber-glue")
public class CommentsPageComponent extends BasePageComponent {
    public CommentsPageComponent(AppiumDriver<? extends MobileElement> driver) {
        super(driver);
    }

    @AndroidFindBy(className = PROGRESS_BAR_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement PROGRESS_BAR;

    @AndroidFindBy(uiAutomator = NO_COMMENTS_ON_THE_ARTICLE_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement NO_COMMENTS_ON_THE_ARTICLE;

    @AndroidFindBy(id = LOAD_MORE_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement LOAD_MORE;

    @AndroidFindBy(id = SET_COMMENT_FIELD_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement SET_COMMENT_FIELD;

    @AndroidFindBy(id = SEND_COMMENT_BTN_ANDROID_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement SEND_COMMENT_BTN;

    @AndroidFindAll({@AndroidBy(id = COMMENTS_CONTENT_ANDROID_LOC)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> COMMENTS_CONTENT;

    @AndroidFindAll({@AndroidBy(id = COMMENTS_DATE_ANDROID_LOC)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> COMMENTS_DATE;

    @AndroidFindAll({@AndroidBy(id = COMMENTS_ANSWER_BTN_ANDROID_LOC)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> COMMENTS_ANSWER_BTN;

    @AndroidFindAll({@AndroidBy(id = COMMENT_AUTHORS_LOC)})
    @iOSFindAll({@iOSBy(id = " ")})
    public static List<MobileElement> COMMENT_AUTHORS;

    @AndroidFindBy(id = REPLY_INFO_LOC)
    @iOSFindBy(id = " ")
    public static MobileElement REPLY_INFO;

    public boolean isCommentsPresentOnScreen() {
        return elementsPresent(COMMENTS_CONTENT, 2);
    }

    public void waitForCommentsIsPresent() {
        int x = 0; // 60 s
        while (!isCommentsPresentOnScreen()) {
            pullToRefreshScreen();
            x++;
            if (x == 30) {
                throw new AssertionError("Комментарии не появились в течение 60 секунд");
            }
        }
    }

    public boolean isProgressBarInvisible() {
        return elementNotPresent(PROGRESS_BAR, 5);
    }

    public boolean isDateFieldHasText(int index, String text) {
        return COMMENTS_DATE.get(index).getText().equalsIgnoreCase(text);
    }

    public boolean isAuthorFieldHasText(int index, String text) {
        return COMMENT_AUTHORS.get(index).getText().equalsIgnoreCase(text);
    }

    public void replyToSomeComment(int index) {
        COMMENTS_ANSWER_BTN.get(index).click();
    }

    public boolean isNoCommentsOnTheArticleTextPresent() {
        return elementPresent(NO_COMMENTS_ON_THE_ARTICLE);
    }

    // ---------------------------------------------------------------------------------


    public boolean theAuthorNameToWhichWeAnswerCorrespondsTo(String authorName) {
        if (REPLY_INFO.getText().equalsIgnoreCase("Ответить " + authorName)) {
            return true;
        } else {
            return false;
        }
    }

    public CommentsPageComponent sendComment(String comment) {
        // sendKeys
        SET_COMMENT_FIELD.setValue(comment);
        SEND_COMMENT_BTN.click();
        return new CommentsPageComponent(getDriver());
    }

    public boolean isCommentPresentInPosition(int position, String comment) {
        if (COMMENTS_CONTENT.get(position).getText().equalsIgnoreCase(comment)) {
            return true;
        } else {
            return false;
        }
    }

    public List<String> getOrderedListOfAllCommentsFromScreen() {
        List<String> list = new ArrayList<>();
        for (MobileElement el : COMMENTS_CONTENT) {
            String text = el.getText();
            list.add(text);
        }
        return list;
    }

    public boolean isCommentPresentInListFromScreen(String text) {
        return isTextPresentIntoMobileElementsOnScreen(COMMENTS_CONTENT, text);
    }

    public boolean isLoadMoreElementPresent(String text) {
        if (elementPresent(LOAD_MORE) && LOAD_MORE.getText().equalsIgnoreCase(text)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isOffsetsMatchPattern(List<Integer> expactedPattern) {
        System.out.println("expacted pattern: " + expactedPattern.toString());
        List<Integer> offsets = new ArrayList<>();
        for (MobileElement element : COMMENTS_CONTENT) {
            offsets.add(element.getLocation().getX());
        }
        System.out.println("offsets all of COMMENTS_CONTENT: " + offsets);
        List<Integer> actualPattern = returningPatternBasedOnTheOffsetOfElements(offsets);
        if (expactedPattern.equals(actualPattern)) {
            return true;
        } else {
            return false;
        }
    }

    private List<Integer> returningPatternBasedOnTheOffsetOfElements(List<Integer> offsets) {
        List<Integer> actualPattern = new ArrayList<>();
        int root = offsets.get(0);
        int index = 0;
        int value = 0;
        for (int x = 0; x < offsets.size(); x++) {
            if (x == 0) {
                value = offsets.get(x);
                index = x;
            } else {
                if (offsets.get(x) > value) {
                    index++;
                    value = offsets.get(x);
                } else if (offsets.get(x) < value && offsets.get(x) != root) {
                    index--;
                    value = offsets.get(x);
                } else if (offsets.get(x) == root) {
                    index = 0;
                    value = offsets.get(x);
                }
            }
            actualPattern.add(index);
        }
        System.out.println("actual: " + actualPattern.toString());
        return actualPattern;
    }

    public boolean isOrderAndContentCommentsCorrectWithScrolling(List<String> listFromDb) {
        List<String> reverseListFromDb = Lists.reverse(listFromDb);
        List<String> listFromUi = new ArrayList<>();
        System.out.println("reverseListFromDb: " + reverseListFromDb.toString());
        for (String text : reverseListFromDb) {

            while (!isTextPresentIntoMobileElementsOnScreen(COMMENTS_CONTENT, text)) {
                swipeUp();
            }

            for (MobileElement elem : COMMENTS_CONTENT) {
                if (elem.getText().equalsIgnoreCase(text)) {
                    listFromUi.add(elem.getText());
                    break;
                }
            }
        }

        System.out.println("listFromUi: " + listFromUi.toString());
        if (reverseListFromDb.equals(listFromUi)) {
            return true;
        } else {
            return false;
        }
    }
}
