package com.appium.test.pages;

import com.appium.test.model.Post;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
@Scope("cucumber-glue")
public class CommonData {
    private UUID uuid = UUID.randomUUID();
    public String randomUUIDString = "TEST" + getDate() + uuid.toString();
    public List<String> tempStringListRootComments = new ArrayList<>();
    public List<String> tempStringListSubComments = new ArrayList<>();
    public String currentAssetId;
    public List<Post> currentListPosts;
    public Post currentTestPost;

    public static String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("_dd/MM/yyyy_HH:mm:ss_");
        Date date = new Date();
        return formatter.format(date);
    }
}
