package com.appium.test.model;

public class Reaction {

    private int value;

    private String label;

    public final int getValue() {
        return this.value;
    }

    public final String getLabel() {
        return  this.label;
    }
}
