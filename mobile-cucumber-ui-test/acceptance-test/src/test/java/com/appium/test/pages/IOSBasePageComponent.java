package com.appium.test.pages;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import org.jsoup.Jsoup;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;

@Component
@Scope("cucumber-glue")

public class IOSBasePageComponent {

    @Autowired
    private FeedPageComponent feedPageComponent;

    public boolean isAppInstalled(String bundleId) {
        JavascriptExecutor js = feedPageComponent.getDriver();
        Map<String, Object> params = new HashMap<>();
        params.put("bundleId", bundleId);
        final boolean isInstalled = (Boolean) js.executeScript("mobile: isAppInstalled", params);
        return isInstalled;
    }

    public boolean scrollIntoContainerByDirectionTo(String direction, MobileElement scrollableContainer) {
        try {
            JavascriptExecutor js = feedPageComponent.getDriver();
            HashMap<String, String> args = new HashMap<>();
            args.put("element", scrollableContainer.getId());
            args.put("direction", direction);
            js.executeScript("mobile: scroll", args);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean scrollIntoContainerToTargetElement(String targetValue, MobileElement scrollableContainer) {
        try {
            JavascriptExecutor js = feedPageComponent.getDriver();
            HashMap<String, String> args = new HashMap<>();
            args.put("element", scrollableContainer.getId());
            args.put("predicateString", "value == '" + targetValue + "'");
            js.executeScript("mobile: scroll", args);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean scrollDown(String value) {
        try {
            MobileElement element = feedPageComponent.getDriver().findElement(MobileBy.name("NewsFeedRootView.tableView"));
            JavascriptExecutor js = feedPageComponent.getDriver();
            HashMap<String, String> args = new HashMap<>();
            args.put("element", element.getId());
            args.put("predicateString", "value == '" + value + "' AND visible == 1");
            js.executeScript("mobile: scroll", args);
            return true;
        } catch (Exception ex) {
            return false;
        }
        // SO_TIMEOUT browserstack
    }

    public void swipe(String direction, int offset, int time) {

        int y = feedPageComponent.getDriver().manage().window().getSize().getHeight();
        int x = feedPageComponent.getDriver().manage().window().getSize().getWidth();
        TouchAction touchAction = new TouchAction(feedPageComponent.getDriver());
//        System.out.println(x + " " + y);
//        System.out.println("Entering swipe");
        if ("right".equalsIgnoreCase(direction)) {
            System.out.println("Swipe Right");
            touchAction.press(point(x - offset, y / 2)).moveTo(point(-(x - (2 * offset)), 0)).release().perform();
        } else if ("left".equalsIgnoreCase(direction)) {
            System.out.println("Swipe Left");
//            touchAction.press(offset, y/2).moveTo((x-(2*offset)), 0).release().perform();
        } else if ("up".equalsIgnoreCase(direction)) {
            System.out.println("Swipe Up");
//            touchAction.press(x/2, offset).moveTo(0, y-(2*offset)).release().perform();
        } else if ("down".equalsIgnoreCase(direction)) {
//            System.out.println("Swipe Down");
            touchAction.press(point(x / 2, y - offset)).moveTo(point(0, -(y - (2 * offset)))).release().perform();
        }
    }


    public void gestSwipeVerticalPercentage(double startPercentage, double finalPercentage, double anchorPercentage, int duration) throws Exception {
        Dimension size = feedPageComponent.getDriver().manage().window().getSize();
        int anchor, startPoint, endPoint;
        anchor = (int) (size.width * anchorPercentage);
        startPoint = (int) (size.height * startPercentage);
        endPoint = (int) (size.height * finalPercentage);
        new TouchAction(feedPageComponent.getDriver()).press(point(anchor, startPoint)).waitAction(waitOptions(Duration.ofMillis(duration)))
                .moveTo(point(anchor, endPoint)).release().perform();
    }
}
