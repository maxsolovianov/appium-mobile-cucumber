package com.appium.test.stepdefs;

import com.appium.test.model.APIRequest;
import com.appium.test.model.Post;
import com.appium.test.pages.CommentsPageComponent;
import com.appium.test.pages.CommonData;
import com.appium.test.utils.MongoUtils;
import com.google.common.collect.Lists;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CommentsStepDefs {
    @Autowired
    private CommentsPageComponent commentsPageComponent;

    @Autowired
    private CommonData commonData;

    @Autowired
    private MongoUtils mongoUtils;

    @When("^user refreshes the screen waiting for comments to appear if there are none$")
    public void userRefreshesTheScreenWaitingForComments() {
        commentsPageComponent.waitForCommentsIsPresent();
    }

    @Then("^no comments are present on screen$")
    public void textIsPresentOnScreen() {
        assert commentsPageComponent.isNoCommentsOnTheArticleTextPresent();
    }

    @Then("^comment '(.*)' is present in the list of comments$")
    public void commentIsPresent(String comment) {
        assert commentsPageComponent.isCommentPresentInListFromScreen(comment);
    }

    @Then("^the (.*) comment in the date field has the text '(.*)'$")
    public void moderationMessageIsPresent(int index, String message) {
        assert commentsPageComponent.isDateFieldHasText(index, message);
    }

    @Then("^the (.*) comment in the date field has not the text '(.*)'$")
    public void moderationMessageIsNotPresent(int index, String text) {
        assert !commentsPageComponent.isDateFieldHasText(index, text);
    }

    @Then("^the name of the author to which the answer corresponds to '(.*)'$")
    public void authorCorrespondsTo(String expactedAuthorName) {
        assert commentsPageComponent.theAuthorNameToWhichWeAnswerCorrespondsTo(expactedAuthorName);
    }

    @When("^user tap replies to the '(.*)' comment$")
    public void userTapRepliesToSpecificComment(int position) {
        commentsPageComponent.replyToSomeComment(position);
    }

    @Given("^created (.*) sub comments to (.*) root on one level as user '(.*)'$")
    public void createdSubcommentsOnOneLevelAsUserServicesForNews(int numberSubcomments, int indexOfRoot, String username) {
        String userId = mongoUtils.getUserId(username);
        commonData.tempStringListRootComments = mongoUtils.insertComments(1, commonData.currentAssetId, userId, null, numberSubcomments);
        // айди рутового, к которому будут добавлены подкомментарии
        String rootCommentId = commonData.tempStringListRootComments.get(indexOfRoot);
        commonData.tempStringListSubComments = mongoUtils.insertComments(numberSubcomments, commonData.currentAssetId, userId, rootCommentId, 0);
    }

    @Given("^created (.*) sub comments to (.*) root as user '(.*)'$")
    public void createdSubcommentsAsUserServicesForNews(int numberSubcomments, int indexOfRoot, String username) {
        List<Post> posts = APIRequest.getOrderedPosts("ru", "latest");
        String userId = mongoUtils.getUserId(username);
        // root comment
        commonData.tempStringListRootComments = mongoUtils.insertComments(1, commonData.currentAssetId, userId, null, numberSubcomments);
        // айди рутового, к которому будут добавлены подкомментарии
        String rootCommentId = commonData.tempStringListRootComments.get(indexOfRoot);
        List<String> id1 = mongoUtils.insertComments(1, commonData.currentAssetId, userId, rootCommentId, 1);
        List<String> id2 = mongoUtils.insertComments(1, commonData.currentAssetId, userId, id1.get(0), 1);
        List<String> id3 = mongoUtils.insertComments(1, commonData.currentAssetId, userId, id2.get(0), 0);
        System.out.println(id3.get(0));

    }

    @Then("^user can see from (.*) to (.*) root comments on screen")
    public void userCanSeeRootCommentsOnScreen(int from, int to) {
        for (int x = from; x <= to; ++x) {
            commentsPageComponent.isCommentPresentInListFromScreen(commonData.tempStringListRootComments.get(x));
            System.out.println(commonData.tempStringListRootComments.get(x));
        }
    }

    @Then("^user can see from (.*) to (.*) sub comments on screen")
    public void userCanSeeSubCommentsOnScreen(int from, int to) {
        for (int x = from; x <= to; ++x) {
            commentsPageComponent.isCommentPresentInListFromScreen(commonData.tempStringListSubComments.get(x));
            System.out.println(commonData.tempStringListSubComments.get(x));
        }
    }

    /**
     * Имеет место ситуация когда комментарий на клиенте появляется через 5 - 10 секунд
     * @param numberOfRootComments
     * @param username
     */
    @Given("^created (.*) root comment as user '(.*)'$")
    public void createdRootCommentAsUserForNews(int numberOfRootComments, String username) {
        String userId = mongoUtils.getUserId(username);
        commonData.tempStringListRootComments = mongoUtils.insertComments(numberOfRootComments, commonData.currentAssetId, userId, null, 0);
    }

    @Then("^the order and content of the root comments is as expected$")
    public void theOrderAndContentOfTheRootCommentsIsAsExpected() {
        List<String> listFromDb = Lists.reverse(commonData.tempStringListRootComments);
        System.out.println("root list of comments from db: " + listFromDb.toString());
        System.out.println("all comments from screen: " + commentsPageComponent.getOrderedListOfAllCommentsFromScreen());
        assert listFromDb.equals(commentsPageComponent.getOrderedListOfAllCommentsFromScreen());
    }

    @Then("^the order and content of the root and sub comments from index (.*) is as expected$")
    public void theOrderAndContentOfTheRootAndSubCommentsIsAsExpected(int index) {
        // аргумент - это позиция, с которой будет вставлен список подкомментариев
        // с экрана вернется общий список
        List<String> allCommentsFromScreen = commentsPageComponent.getOrderedListOfAllCommentsFromScreen();
        System.out.println("список всех комментариев с экрана: " + allCommentsFromScreen.toString());
        // нужно сделать такой же общий список из рутовых и подкомментариев, добавленных в базу
        // вставить список подкомментариев в середину рутового списка от комментария к которому добавлены
        List<String> listOfRootComments = commonData.tempStringListRootComments;
        List<String> listOfSubcomments = commonData.tempStringListSubComments;
        // Inserts all of the elements in the specified collection into this list, starting at the specified position
        listOfRootComments.addAll(index, listOfSubcomments);
        System.out.println("список рутовых комментариев в базу: " + listOfRootComments.toString());
        assert allCommentsFromScreen.equals(listOfRootComments);
    }

    @Then("^user can see (.*) element$")
    public void userCanSeeElement(String text) {
        assert commentsPageComponent.isLoadMoreElementPresent(text);
    }

    @Then("^user can not see latest sub comment from index (.*)$")
    public void userCanNotSeeLatestComment(int index) {
        List<String> allCommentsFromScreen = commentsPageComponent.getOrderedListOfAllCommentsFromScreen();
        System.out.println("список всех комментариев с экрана: " + allCommentsFromScreen.toString());
        List<String> listOfRootComments = commonData.tempStringListRootComments;
        List<String> listOfSubcomments = commonData.tempStringListSubComments;
        listOfRootComments.addAll(index, listOfSubcomments);
        System.out.println("список рутовых комментариев в базу: " + listOfRootComments.toString());
        // get latest from List
        String latestComment = listOfRootComments.get(listOfRootComments.size() - 1);
        System.out.println("latest comment is: " + latestComment);

    }

    @Then("^the offset of the list of comments corresponds to the pattern$")
    public void theOffsetOfTheListOfCommentsCorrespondsToThePattern(List<Integer> list) {
        assert commentsPageComponent.isOffsetsMatchPattern(list) : "Смещение в дереве комментариев не соответствует паттерну";
    }

    // !!!
    @When("^user tap (.*) element$")
    public void userTapElement(String elText) {
        if (commentsPageComponent.LOAD_MORE.getText().equalsIgnoreCase(elText)) {
            commentsPageComponent.LOAD_MORE.click();
        }
    }

    @When("^user setting up the comment '(.*)' to comment with index (.*)$")
    public void userSetCommentToNewsIndex(String comment, int index) {
        commentsPageComponent.COMMENTS_ANSWER_BTN.get(index).click();
        commentsPageComponent.SET_COMMENT_FIELD.setValue(comment);
        commentsPageComponent.SEND_COMMENT_BTN.click();
    }

    @Given("^there are no test comments for news$")
    public void thereAreNoTestCommentsForNews() {
        mongoUtils.clearAllTestDataFromCommentsCollection(commonData.currentAssetId);
    }

    @Then("^the user scrolls and sees all comments in the correct order$")
    public void theUserScrollsAndSeesCommentsInTheCorrectOrder() {
        assert commentsPageComponent.isOrderAndContentCommentsCorrectWithScrolling(commonData.tempStringListRootComments);
    }

    @When("^scroll down the screen to activate pagination$")
    public void scrollDownTheScreenToActivatePagination() {
        commentsPageComponent.swipeUp();
        commentsPageComponent.swipeUp();
        commentsPageComponent.swipeUp();
        commentsPageComponent.swipeDown();
        commentsPageComponent.swipeDown();
        commentsPageComponent.swipeDown();
    }
}
