package com.appium.test.stepdefs;

import com.appium.test.model.APIRequest;
import com.appium.test.model.Post;
import com.appium.test.pages.CommonData;
import com.appium.test.pages.FeedPageComponent;
import com.appium.test.pages.LangPageComponent;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class FeedStepDefs {

    @Autowired
    private FeedPageComponent feedPageComponent;

    @Autowired
    private LangPageComponent langPageComponent;

    @Autowired
    private CommonData commonData;

    // TODO:
    // перейти к категории
    // добавить в закладки новость
    // получить дату публикации для новости
    // получить просмотры для новости
    // открыть новость +

    private static String html2string(String html) {
        return Jsoup.parse(html).text();
    }

    @Given("^interface language is '(.*)'$")
    public void selectLanguage(String language) {
        langPageComponent.selectInterfaceLanguage(language);
    }

    @When("^the user finds and opens the news article$")
    public void theUserFindsAndOpensTheNewsArticle() {
        feedPageComponent.theUserFindsAndOpensTheNewsArticleByTitle(commonData.currentTestPost.getTitle());
    }
}


