package com.appium.test.utils;

import org.bson.BsonNull;

import java.util.Date;

public class StatusHistory {

    private String type;

    // Date
    private Date created_at;

    // Null
    private BsonNull assigned_by;

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BsonNull getAssigned_by() {
        return assigned_by;
    }

    public void setAssigned_by(BsonNull assigned_by) {
        this.assigned_by = assigned_by;
    }
}
