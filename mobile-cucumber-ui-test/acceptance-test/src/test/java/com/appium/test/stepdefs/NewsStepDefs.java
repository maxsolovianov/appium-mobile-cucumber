package com.appium.test.stepdefs;

import com.appium.test.pages.*;
import com.appium.test.model.*;
import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.appium.test.pages.NewsPageComponent.*;
import static org.junit.Assert.assertTrue;

public class NewsStepDefs {

    private static final Logger LOG = Logger.getLogger(NewsStepDefs.class);

    @Autowired
    private NewsPageComponent newsPageComponent;

    @Autowired
    private FacebookLoginPageComponent facebookLoginPageComponent;

    @Autowired
    private CommonData commonData;

    // проверить, будет ли инициализирован бин, если нет вызова методов объекта
    // будет, инициализируется переменная бином из контейнера
//    @Autowired
//    private MongoUtils mongoUtils;

    @When("^user sent the '(.*)' comment under the current news$")
    public void userSentCommentOnCurrentNews(String comment) {
        newsPageComponent.goToCommentsScreen().sendComment(comment);
    }

    @When("^user goes to comments screen$")
    public void userGoesToCommentsScreen() {
        assert newsPageComponent.isPostPlaceholderNotPresent() :
                "Элемент POST_PLACEHOLDER не исчез в течение 5 сек";
        newsPageComponent.goToCommentsScreen();
    }

    @Then("^the title inside the body of the news is as expected$")
    public void theTitleInsideTheBodyOfTheNewsIsAsExpected() {
        String title = commonData.currentTestPost.getTitle();
        assert newsPageComponent.theTitleInsideTheBodyOfTheNewsIsAsExpected(title);
    }

    @Then("^user sees all news content items$")
    public void userSeesAllNewsContentItems() {
        List<ContentItem> items = commonData.currentTestPost.getContentItem();
        newsPageComponent.userSeesAllNewsContentItems(items);
    }

    @Then("^the publish date inside the body of the news is as expected$")
    public void thePublishDateInsideTheBodyOfTheNewsIndexIsAsExpected() {
        String publishDateFromApi = commonData.currentTestPost.getPublishDate();
        String expectedDate = newsPageComponent.convertDateFromApiToUi(publishDateFromApi);
        String dateFromUi = newsPageComponent.getTextIfPresent(TV_DATE_INTO_BODY, 2);
        LOG.info("Converted date from Api: " + expectedDate);
        LOG.info("Raw date from UI:        " + dateFromUi);
        // временное решение! нужно сравнивать на эквивалентность, а не вхождение подстроки.
        assertTrue(dateFromUi.contains(expectedDate));
//        assertEquals(expectedDate, dateFromUi);
    }

    @Then("^the views inside the body of the news is as expected$")
    public void theViewsInsideTheBodyOfTheNewsIndexIsAsExpected() {
        int viewsApi = commonData.currentTestPost.getViews();
        LOG.info("views from API: " + viewsApi);
        int viewsUi = Integer.parseInt(newsPageComponent.getTextIfPresent(TV_NEWS_POST_VIEW, 5));
        LOG.info("Views from UI: " + viewsUi);
        try {
            assertTrue(viewsApi == viewsUi || viewsApi == viewsUi - 1 || viewsApi == viewsUi + 1);
        } catch (java.lang.AssertionError er) {
            LOG.info("API views: " + viewsApi);
            LOG.info("UI  views: " + viewsUi);
        }
    }

    @When("^user try to share through Facebook app$")
    public void userTryToShareThroughFacebookApp() {
        newsPageComponent.userTryToShareThroughFacebookApp();
    }

    @When("^user logs in into FB account to share$")
    public void userLogsInIntoFBAccountToShare() {
//        newsPageComponent.switchContextToFirstWEBVIEW();
        facebookLoginPageComponent.createTestAccount();
        facebookLoginPageComponent.acceptPrivacyPolicy();
        facebookLoginPageComponent.typeUserName();
        facebookLoginPageComponent.typePassword();
        facebookLoginPageComponent.submitLogin();
    }

    @When("^user share story$")
    public void userShareStory() {
        facebookLoginPageComponent.typeMessageToFeedWithPost();
        facebookLoginPageComponent.checkTypedMessage();
        facebookLoginPageComponent.publishPost();
        facebookLoginPageComponent.elementSendingPresent();
    }

    @Then("^shared story is present in the feed$")
    public void sharedStoryIsPresentInTheFeed() {
        facebookLoginPageComponent.checkThatMessageInFeedMatchesTheExpected();
        facebookLoginPageComponent.removeTestAccount();
    }

    @Then("^user sees all image captions$")
    public void userSeesAllImageCaptionsForNewsIndex() {
        // ожидать невидимости плейсхолдеров (дожидаемся загрузки картинок)
        assert newsPageComponent.userSeesAllImageCaptions(commonData.currentTestPost.getContentItem()) :
                "Подписи под изображениями не соответствуют ожидаемым";
    }

    @Then("^reactions for the news is correct$")
    public void reactionsForTheNewsIsCorrect() {
        assert newsPageComponent.reactionsForTheNewsIsCorrect(commonData.currentTestPost.getReactionList()) :
                "Список реакций не соответствует ожидаемому";
    }
}
