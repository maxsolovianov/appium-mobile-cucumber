package com.appium.framework.driver;

import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

import static com.appium.framework.core.AppiumCukes.APPIUM_PORT;
import static com.support.framework.support.Property.*;
import static io.appium.java_client.remote.AutomationName.IOS_XCUI_TEST;

final class TestCapabilities {

    private static final Logger LOG = Logger.getLogger(TestCapabilities.class);

    static DesiredCapabilities getDesiredCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        switch (DEVICE_LOCATION.toString().toUpperCase()) {
            case "LOCAL":
                try {
                    capabilities.setCapability(MobileCapabilityType.APP,
                            new File(URLDecoder.decode(ClassLoader.getSystemResource((APP_FILE.toString())).getFile(),
                                    StandardCharsets.UTF_8.toString())).getAbsolutePath());
                } catch (NullPointerException e) {
                    Assert.fail("Cannot find given application local file" + e);
                } catch (UnsupportedEncodingException e) {
                    LOG.warn(e);
                }
                break;
            case "CLOUD":
                try {
                    capabilities.setCapability(MobileCapabilityType.APP, APP_FILE);
                } catch (NullPointerException e) {
                    Assert.fail("Cannot find given application file in cloud storage" + e);
                }
                break;
        }

        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PLATFORM_NAME);
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, DEVICE_NAME);
        capabilities.setCapability(MobileCapabilityType.UDID, DEVICE_NAME);
        capabilities.setCapability(MobileCapabilityType.NO_RESET, NO_RESET.toBoolean());
        capabilities.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.9.1");
//        capabilities.setCapability(MobileCapabilityType.FULL_RESET, FULL_RESET.toBoolean());
//        capabilities.setCapability("browserstack.debug", true);
        // BROWSERSTACK
        capabilities.setCapability("browserstack.deviceLogs", false);
        capabilities.setCapability("nativeWebScreenshot", false);
        capabilities.setCapability("browserstack.networkLogs", false);
        capabilities.setCapability("browserstack.seleniumLogs", false);
        capabilities.setCapability("browserstack.appiumLogs", false);
        capabilities.setCapability("browserstack.debug", false);
        // Alerts
        // all alerts in the app get auto accepted
        capabilities.setCapability("autoAcceptAlerts", true);
        // or
//        capabilities.setCapability("autoDismissAlerts", true);


        switch (PLATFORM_NAME.toString().toUpperCase()) {
            case "IOS":
//                XCUITest driver
                capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, IOS_XCUI_TEST);
                capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, PLATFORM_VERSION);
                capabilities.setCapability("xcodeOrgId", XCODE_ORG_ID);
                capabilities.setCapability("xcodeSigningId", XCODE_SIGNING_ID);
                capabilities.setCapability("useNewWDA", true);
                capabilities.setCapability("showXcodeLog", false);
                capabilities.setCapability("showIOSLog", false);
//                getting of source 2x or 3x faster
                capabilities.setCapability("useJSONSource", true);
//                for performance
//                !!! there is side effect that scroll does not work when try to scroll to invisible component that are off screen
                capabilities.setCapability("simpleIsVisibleCheck", true);
                capabilities.setCapability("preventWDAAttachments", true);
//                capabilities.setCapability("keychainPath", "/Users/solovianov/Library/Keychains/MyKeychain.keychain-db");
//                capabilities.setCapability("keychainPassword", "vector");
                capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 120000);
                capabilities.setCapability("launchTimeout", 120000);
                capabilities.setCapability("webkitResponseTimeout", 120000);
                capabilities.setCapability("idleTimeout", 120000);
                break;
            case "ANDROID":
//                UiAutomator2 driver
//                capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
//                capabilities.setCapability("browserstack.networkLogs", BROWSERSTACK_NETWORK_LOGS.toBoolean());
                // использовать хромдрайвер что был загружен с аппиум
                capabilities.setCapability("chromedriverUseSystemExecutable", true);
                capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY, APP_WAIT_ACTIVITY);
//                capabilities.setCapability(AndroidMobileCapabilityType.NATIVE_WEB_SCREENSHOT, NATIVE_WEB_SCREENSHOT.toBoolean());
//                automatically accept any permission alerts - sms,location and etc:
                capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
//                capabilities.setCapability(AndroidMobileCapabilityType.SUPPORTS_ALERTS, true);
                break;
            default:
                Assert.fail("Current test platform is not supported: " + PLATFORM_NAME);
        }
        return capabilities;
    }

    static URL getUrl() {
        try {
            if (DEVICE_LOCATION.toString().toUpperCase().equalsIgnoreCase("CLOUD")) {
                return new URL("http://" + BROWSERSTACK_USER.toString() + ":" + BROWSERSTACK_KEY.toString() + "@hub-cloud.browserstack.com/wd/hub");
            } else {
                return new URL("http://" + APPIUM_HOST.toString() + ":" + APPIUM_PORT + "/wd/hub");
            }

        } catch (MalformedURLException e) {
            Assert.fail("Cannot initiate REST http interface listener URL");
            return null;
        }
    }
}

