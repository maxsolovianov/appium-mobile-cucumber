package com.appium.framework.base;

import com.support.framework.base.DriverInterface;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.touch.WaitOptions;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.PageFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.Duration;

import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofSeconds;

@Component
@Scope("cucumber-glue")
public class AppiumBase extends AbstractAppiumBase implements DriverInterface<MobileElement> {

    private final double FIRST_MULTIPLIER = 0.80;
    private final double SECOND_MULTIPLIER = 0.20;

    private final double FIRST_MULTIPLIER_LEFTRIGHT = 0.95;
    private final double SECOND_MULTIPLIER_LEFTRIGHT = 0.05;

    public AppiumBase(AppiumDriver<? extends MobileElement> driver) {
        super(driver);
        initPageFactoryElements(this);
    }

    @Override
    public void initPageFactoryElements(Object object) {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver()), object);
    }

    @Override
    public void longPress(MobileElement element, int seconds) {
        TouchAction action = new TouchAction(getDriver());
//        action.longPress(element, seconds).perform().release();
    }

    public void goBack() {
        getDriver().navigate().back();
    }

    // close application and open it again
    public void resetApp() {
        getDriver().resetApp();
    }

    @Override
    public void swipeUp() {
        swipeUpOrDown(FIRST_MULTIPLIER, SECOND_MULTIPLIER);
    }

    @Override
    public void swipeDown() { swipeUpOrDown(SECOND_MULTIPLIER, FIRST_MULTIPLIER); }

    @Override
    public void swipeLeft() {
        swipeLeftOrRight(FIRST_MULTIPLIER_LEFTRIGHT, SECOND_MULTIPLIER_LEFTRIGHT);
    }

    @Override
    public void swipeRight() {
        swipeLeftOrRight(SECOND_MULTIPLIER_LEFTRIGHT, FIRST_MULTIPLIER_LEFTRIGHT);
    }

    private void swipeLeftOrRight(double first, double second) {
        Dimension dimensions = getDriver().manage().window().getSize();
        int startX = (int) (dimensions.width * first);
        int endX = (int) (dimensions.width * second);
        int startY = dimensions.height / 2;
        TouchAction touchAction = new TouchAction(getDriver());
        touchAction.longPress(point(startX, startY)).moveTo(point(endX, startY)).release().perform();
    }

    private void swipeUpOrDown(double first, double second) {
        Dimension dimensions = getDriver().manage().window().getSize();
        int startY = (int) (dimensions.getHeight() * first);
        int endY = (int) (dimensions.getHeight() * second);
        int startX = dimensions.width / 2;
        TouchAction touchAction = new TouchAction(getDriver());
        touchAction.longPress(point(startX, startY)).moveTo(point(startX, endY)).release().perform();
    }
}
