package com.appium.framework.core;

import com.support.framework.support.Property;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import static com.appium.framework.core.AppiumServer.startAppiumServer;
import static com.appium.framework.core.AppiumServer.stopAppiumServer;
import static com.support.framework.support.CucumberReporting.generateReports;
import static com.support.framework.support.Property.*;
import static com.support.framework.support.Util.getPort;

// Cucumber Runner
// using Cucumber Junit runner
@RunWith(Cucumber.class)
@CucumberOptions(
        // redirect formatter (not only JSON but pretty, progress, html, etc. as well)
        plugin = "json:target/cucumber.json",
        features = {"classpath:features"},
        // классы с реализацией шагов и «хуков»
        glue = {"com.appium.test.stepdefs", "com.appium.framework.hook"},
        tags = {"~@ignore"},
        // false: предупреждение будет выдаваться по достижении неразработанного шага
        dryRun = false,
        // false — неразработанные шаги пропускаются
        strict = false)

public class AppiumCukes {

    private static final Logger LOG = Logger.getLogger(AppiumCukes.class);
    public static int APPIUM_PORT;

    @BeforeClass
    public static void startAppium() {

        if (PLATFORM_NAME.toString().equalsIgnoreCase("android") && DEVICE_NAME.toString().contains("qa-devicefarm")) {
            LOG.info("### Trying ADB Connect To QA Farm Device ###");
            try {
                Runtime rt = Runtime.getRuntime();
                rt.exec("adb connect " + DEVICE_NAME.toString());
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

        if (DEVICE_LOCATION.toString().equalsIgnoreCase("local")) {
            LOG.info("### Starting Appium ###");
            APPIUM_PORT = getPort(Property.APPIUM_PORT.toInt());
            LOG.info("Appium Host: " + APPIUM_HOST + " & Port: " + APPIUM_PORT + " & Log Level: " + APPIUM_LOG);
            LOG.info("Platform: " + PLATFORM_NAME + " & Test Device: " + DEVICE_NAME);
            LOG.info("Keep App State Between Scenarios: " + NO_RESET + " & Compare Image Status: " + COMPARE_IMAGE);
            startAppiumServer();
        } else {
            //LOG.info("### Connecting to https://www.browserstack.com/ ###");
            //LOG.info("### Working with remote Appium server across Mobile JSON Wire Protocol ###");
        }
    }

    @AfterClass
    public static void stopAppium() {
        if (DEVICE_LOCATION.toString().equalsIgnoreCase("local")) {
            LOG.info("### Stopping Appium ###");
            stopAppiumServer();
        } else {
            //LOG.info("Stopping session with remote Appium server");
        }
        generateReports();
    }
}