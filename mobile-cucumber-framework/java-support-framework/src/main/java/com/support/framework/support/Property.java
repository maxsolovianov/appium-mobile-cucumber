package com.support.framework.support;

import org.junit.Assert;

import java.util.Optional;

import static com.support.framework.support.Util.stringIsEmpty;

public enum Property {

//    public static final String USERNAME = System.getenv("BROWSERSTACK_USER");
//    public static final String AUTOMATE_KEY = System.getenv("BROWSERSTACK_ACCESS_KEY");

    DEVICE_LOCATION(System.getProperty("device.location")),
    BROWSERSTACK_USER(System.getProperty("browserstack.user")),
    BROWSERSTACK_KEY(System.getProperty("browserstack.key")),
    BROWSERSTACK_NETWORK_LOGS(System.getProperty("browserstack.network.logs")),
    PLATFORM_NAME(System.getProperty("platform.name")),
    PLATFORM_VERSION(System.getProperty("platform.version")),
    IMPLICIT_WAIT(Optional.ofNullable(System.getProperty("implicit.wait")).orElse("5")),
    COMPARE_IMAGE(Optional.ofNullable(System.getProperty("compare.image")).orElse("false")),
    MOBILE_API_HOST(System.getProperty("mobile.api.host")),
    NEWS_LIST_ENDPOINT(System.getProperty("newslist.endpoint")),
    CATEGORIES_ENDPOINT(System.getProperty("categories.endpoint")),

    //Appium Specific
    APP_FILE(System.getProperty("app.file")),
    APP_WAIT_ACTIVITY(System.getProperty("NurKZ2.app.wait.activity")),
    APPIUM_LOG_PATH(System.getProperty("appium.log.path")),
    CHROMEDRIVER_EXECUTABLE(System.getProperty("chromedriver.executable")),
    DEVICE_NAME(System.getProperty("device.name")),
    APPIUM_HOST(Optional.ofNullable(System.getProperty("appium.host")).orElse("127.0.0.1")),
    APPIUM_PORT(Optional.ofNullable(System.getProperty("appium.port")).orElse("0")),
    NO_RESET(Optional.ofNullable(System.getProperty("no.reset")).orElse("true")),
    FULL_RESET(Optional.ofNullable(System.getProperty("full.reset")).orElse("true")),
    IGNORE_UNIMPORTANT_VIEWS(Optional.ofNullable(System.getProperty("ignore.unimportant.views")).orElse("true")),
    NATIVE_WEB_SCREENSHOT(Optional.ofNullable(System.getProperty("native.web.screenshot")).orElse("false")),
    APPIUM_LOG(Optional.ofNullable(System.getProperty("appium.log")).orElse("warn")),
    XCODE_ORG_ID(System.getProperty("xcode.org.id")),
    XCODE_SIGNING_ID(System.getProperty("xcode.signing.id")),

    TESTRAIL_URL(System.getProperty("testrail.url"));

    private String value;

    Property(String value) {
        this.value = value;
    }

    public boolean toBoolean() {
        if (stringIsEmpty(value)) {
            Assert.fail("Property " + this.name() + " is missing. Check your your pom.xml");
        }
        return Boolean.parseBoolean(value);
    }

    public int toInt() {
        if (stringIsEmpty(value)) {
            Assert.fail("Property " + this.name() + " is missing. Check your your pom.xml");
        }
        return Integer.parseInt(value);
    }

    public String toString() {
        if (stringIsEmpty(value)) {
            Assert.fail("Property " + this.name() + " is missing. Check your your pom.xml");
        }
        return value;
    }
}



