package com.support.framework.support;

import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Matcher {

    private static final Logger LOG = Logger.getLogger(Matcher.class);

    public static String doMatch(String inputSequence, String strPattern) {

        List<String> allMatchesList = new ArrayList<>();

        Pattern pattern = Pattern.compile(strPattern);

        java.util.regex.Matcher matcher = pattern.matcher(inputSequence);

        while (matcher.find()) {
            allMatchesList.add(matcher.group());
        }

        StringBuilder sb = new StringBuilder();
        for (String eachInList : allMatchesList) {
            sb.append(eachInList);
        }

        //LOG.info("Output String after matching : " + sb.toString());

        return sb.toString();
    }
}
